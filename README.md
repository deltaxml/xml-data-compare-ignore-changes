# Ignore Changes

See [the sample page](https://docs.deltaxml.com/xml-data-compare/latest/ignore-changes) on the website for more details about the implications of ignoring changes.

These sample files demonstrate the effect of using the ignore-changes parameter in the configuration file.  If the A and B files are compared with the empty config file, then customerid 16 (Sherlock Holmes) will show several differences in the 'extra' element.  

If you do not require to show differences when they occur in a particular element, then use the ignore-changes parameter:  



```
  <dcf:location name="Extra" xpath="//extra">
    <dcf:ignore-changes use="B"/>
  </dcf:location>
```
You can decide whether to show the data from the A or B file with the 'use' attribute.  For the full list of options see the ignore-changes entry in the [XML Data Compare Configuration File Schema Guide](https://docs.deltaxml.com/xml-data-compare/latest/configuration-file-schema-guide)

## REST request:

Replace {LOCATION} below with your location of the download. 



```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-ignore-changes.xml</path>
    </configuration>
</compare>
```

See the [XML Data Compare REST User Guide](https://docs.deltaxml.com/xml-data-compare/latest/rest-user-guide) for how to run the comparison using REST.